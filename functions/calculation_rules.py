"""
File consists of several functions for the calculation rules of FAIR Quality KPIs
"""
import numpy as np
from scipy.integrate import odeint

import matplotlib.pyplot as plt

def test_function():
    """Test function to check module functionality"""
    print("You called the test function.")


def kpi_sum(*args):
    """
    Calculates the sum of one or more integers or lists.

    Args:
        *args: One or more integers or lists.

    Returns:
        total (int): The sum of all given integers and/or the sum
        of all items in all given lists.

    Raises:
        TypeError: If an argument with unsupported type is passed
            (i.e. anything other than int or list).
    """
    total = 0
    for arg in args:
        if isinstance(arg, int):  # Check if argument is an integer
            total += arg
        elif isinstance(arg, list):  # Check if argument is a list
            total += sum(arg)
        else:
            raise TypeError(
                f"Unsupported type {type(arg)} passed to kpi_sum()")
    return total

    # if arguments are handed over not as a list: sum(list(args))
    
    


# Add new functions for calculating some real complicated metrics

def calculate_total_cost(car):
    # Set cost to zero
    total_cost = 0
    # Add cost of each component to total cost.
    for component in car.get_component_list(-1):
        total_cost += component.properties["price [Euro]"]    
    return total_cost

def calculate_delivery_time(car):
    # Set delivery time to zero 
    delivery_time = 0
    # Loop through all parts, if one has longer delivery time than current delivery time replace it.
    for component in car.get_component_list(-1):
        if component.properties["delivery time [days]"] > delivery_time:
            delivery_time = component.properties["delivery time [days]"]
        else:
            pass
    return delivery_time

def calculate_number_of_parts(car):
    # Get length of component list
    number_of_parts = len(car.get_component_list(-1))
    return number_of_parts

def calculate_total_weight(car):
    # Set weight to zero
    total_weight = 0
    # Add weight of each component to total weight
    for component in car.get_component_list(-1):
        total_weight += component.properties["mass [g]"]    
    return total_weight
    



    
    





###

if __name__ == "__main__":
    """
    Function to inform that functions in this module is
    intended for import not usage as script
    """
    print(
        "This script contains functions for calculating the FAIR Quality KPIs."
        "It is not to be executed independently."
    )
