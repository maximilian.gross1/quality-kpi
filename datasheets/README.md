# Hinweise zu Teilenummern in LeoCAD

Einige Teile der `.json`-Dateien lassen sich nicht direkt in der Standardbibliothek von LeoCAD wiederfinden. Diese sind dort mit abweichenden Nummern hinterlegt. Auf den Raspberry Pis wurden die von uns verwendeten Teilnummern nachgetragen und lassen sich daher wie erwartet auffinden. Falls Sie LeoCAD auf Ihrem eigenen Rechner verwenden, beachten Sie die folgenden Korrespondenztabellen:

Batterien:

Teilnummer `.json`-Datei | Teilnummer LeoCAD
-------------------------|------------------
8878-1                   | 84599
8881-1                   | 58119
88000-1                  | 64228

Motoren:

Teilnummer `.json`-Datei | Teilnummer LeoCAD
-------------------------|------------------
8882-1                   | 58121
8883-1                   | 58120
88003-1                  | 99499

Räder:

Teilnummer `.json`-Datei | Teilnummer(n) LeoCAD
-------------------------|-------------------------------
4265cc01                 | 32123a (Felge), 59895 (Reifen)
56904c02                 | 56904 (Felge), 30699 (Reifen)
41896c04                 | 41896c01
2903c02                  | 2903 (Felge), 6596 (Reifen)